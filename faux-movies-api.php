<?php

$movies = [
  [
    'uid' => '1',
    'title' => 'Spider-Man',
    'year' => 2002,
  ],
  [
    'uid' => '2',
    'title' => 'Spider-Man 2',
    'year' => 2004,
  ],
  [
    'uid' => '3',
    'title' => 'Spider-Man 3',
    'year' => 2007,
  ],
  [
    'uid' => '4',
    'title' => 'The Amazing Spider-Man',
    'year' => 2012,
  ],
  [
    'uid' => '5',
    'title' => 'The Amazing Spider-Man 2',
    'year' => 2014,
  ],
  [
    'uid' => '6',
    'title' => 'Spider-Man: Homecoming',
    'year' => 2017,
  ],
  [
    'uid' => '7',
    'title' => ' Spider-Man: Far From Home',
    'year' => 2019,
  ],
  [
    'uid' => '8',
    'title' => ' Spider-Man: No Way Home',
    'year' => 2021,
  ],
  [
    'uid' => '9',
    'title' => 'Spider-Man: Into the Spider-Verse',
    'year' => 2018,
  ],
  [
    'uid' => '10',
    'title' => 'Spider-Man: Across the Spider-Verse (Part One)',
    'year' => 2022,
  ],
  [
    'uid' => '11',
    'title' => 'Spider-Man: Across the Spider-Verse (Part Two)',
    'year' => 2023,
  ],
  [
    'uid' => '12',
    'title' => 'Spider-Ham: Caught in a Ham',
    'year' => 2019,
  ],
  [
    'uid' => '13',
    'title' => 'Avengers: Infinity War',
    'year' => 2018,
  ],
  [
    'uid' => '14',
    'title' => 'Avengers: Endgame',
    'year' => 2019,
  ],
  [
    'uid' => '15',
    'title' => 'Captain America: Civil War',
    'year' => 2016,
  ],
  [
    'uid' => '16',
    'title' => 'Marvel Super Heroes 4D',
    'year' => 2010,
  ],
  [
    'uid' => '17',
    'title' => 'Phineas and Ferb: Mission Marvel',
    'year' => 2013,
  ],
  [
    'uid' => '18',
    'title' => 'Rat Pfink a Boo Boo',
    'year' => 1966,
  ],
  [
    'uid' => '19',
    'title' => 'Flash Gordon',
    'year' => 1980,
  ],
  [
    'uid' => '20',
    'title' => 'Teenage Mutant Ninja Turtles',
    'year' => 1990,
  ],
  [
    'uid' => '21',
    'title' => 'Teenage Mutant Ninja Turtles II: The Secret of the Ooze',
    'year' => 1991,
  ],
  [
    'uid' => '22',
    'title' => 'Teenage Mutant Ninja Turtles III',
    'year' => 1993,
  ],
  [
    'uid' => '23',
    'title' => 'Unbreakable',
    'year' => 2000,
  ],
  [
    'uid' => '24',
    'title' => 'Hulk',
    'year' => 2003,
  ],
  [
    'uid' => '25',
    'title' => 'Fantastic Four',
    'year' => 2005,
  ],
  [
    'uid' => '26',
    'title' => 'Iron Man',
    'year' => 2008,
  ],
  [
    'uid' => '27',
    'title' => 'Thor',
    'year' => 2011,
  ],
  [
    'uid' => '28',
    'title' => 'Captain America: The First Avenger',
    'year' => 2011,
  ],
  [
    'uid' => '29',
    'title' => 'The Avengers',
    'year' => 2012,
  ],
  [
    'uid' => '30',
    'title' => 'Ant-Man',
    'year' => 2015,
  ],
  [
    'uid' => '31',
    'title' => 'Black Panther',
    'year' => 2018,
  ],
  [
    'uid' => '32',
    'title' => 'Black Widow',
    'year' => 2021,
  ],
  [
    'uid' => '33',
    'title' => 'Batman',
    'year' => 1966,
  ],
  [
    'uid' => '34',
    'title' => 'Superman',
    'year' => 1978,
  ],
  [
    'uid' => '35',
    'title' => 'Swamp Thing',
    'year' => 1982,
  ],
  [
    'uid' => '36',
    'title' => 'Batman Returns',
    'year' => 1992,
  ],
  [
    'uid' => '37',
    'title' => 'Batman Forever',
    'year' => 1995,
  ],
  [
    'uid' => '38',
    'title' => 'Batman Begins',
    'year' => 2005,
  ],
  [
    'uid' => '39',
    'title' => 'The Dark Knight',
    'year' => 2008,
  ],
  [
    'uid' => '40',
    'title' => 'Green Lantern',
    'year' => 2011,
  ],
  [
    'uid' => '41',
    'title' => 'Wonder Woman',
    'year' => 2017,
  ],
  [
    'uid' => '42',
    'title' => 'The Batman',
    'year' => 2022,
  ],
];

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  $substr = $_GET['substr'];

  $matched_movies = array_filter($movies, function ($movie) use ($substr) {
    $title = $movie['title'];

    if (is_string($title) && is_string($substr)) {
      return strpos(strtolower($title), strtolower($substr)) !== false;
    }

    return false;
  });

  $num_results_per_page = 10;
  $total_num_results = count($matched_movies);
  $total_num_pages = ceil($total_num_results / $num_results_per_page);

  $page = (isset($_GET['page']) && is_numeric($_GET['page'])) ?
    intval($_GET['page']) : 1;
  $page = min($page, $total_num_pages);
  $page = max($page, 1);

  $first_result_index = ($page - 1) * $num_results_per_page;

  $results = array_slice(
    $matched_movies,
    $first_result_index,
    $num_results_per_page
  );

  $num_results = count($results);

  header('Content-Type: application/json; charset=utf-8');
  echo json_encode([
    'totalNumResults' => $total_num_results,
    'page' => $page,
    'totalNumPages' => $total_num_pages,
    'numResultsPerPage' => $num_results_per_page,
    'numResults' => $num_results,
    'results' => $results,
  ]);
}
